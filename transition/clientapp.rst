クライアント用アプリ 画面遷移
=============================

.. blockdiag::

   blockdiag clientapp_transition {
        login [label = "ログイン"];

        adv_page [label = "システム紹介ページ"];
        sign_up [label = "サインアップ"];

        app_main_page [label = "Appメインページ"];
        app_info_list [label = "Appお知らせリスト"];
        app_info_detail [label = "Appお知らせ詳細"];

        app_member_mypage [label = "Appメンバーマイページ"];
        app_member_mypage_edit [label = "Appメンバーマイページ編集"];

        app_equipmgmt_main [label = "App備品管理メイン"];
        app_equipmgmt_tag_reader_1 [label = "App備品管理タグ読み取り"];
        app_equipmgmt_tag_reader_2 [label = "App備品管理タグ読み取り"];
        app_equipmgmt_reserve [label = "App備品貸出予約"];
        app_equipmgmt_reserve_confirm [label = "App備品貸出予約確認"];
        //app_equipmgmt_reserve_comp [label = "App備品貸出予約完了"];
        app_equipmgmt_return [label = "App備品返却登録"];
        app_equipmgmt_return_confirm [label = "App備品返却登録確認"];
        //app_equipmgmt_return_comp [label = "App備品返却登録完了"];

        app_income_report_view [label = "App収支報告表示"];

        app_activities_report_view [label = "App活動報告表示"];

        app_push_msg_view [label = "Appプッシュ通知表示"];

        adv_page <-> sign_up
        adv_page <-> login

        // お知らせ表示
        login <-> app_main_page <-> app_info_list <-> app_info_detail
                  app_main_page <-> app_info_detail
                  // マイページ
                  app_main_page <-> app_member_mypage -> app_member_mypage_edit
                  // 備品管理
                  app_main_page <-> app_equipmgmt_main <-> app_equipmgmt_reserve <-> app_equipmgmt_tag_reader_1 -> app_equipmgmt_reserve_confirm
                                    app_equipmgmt_main <-> app_equipmgmt_reserve -> app_equipmgmt_reserve_confirm
                                    app_equipmgmt_main <-> app_equipmgmt_return <-> app_equipmgmt_tag_reader_2 -> app_equipmgmt_return_confirm
                                    app_equipmgmt_main <-> app_equipmgmt_return -> app_equipmgmt_return_confirm
                  // 収支報告
                  app_main_page <-> app_income_report_view
                  // 活動報告表示
                  app_main_page <-> app_activities_report_view
        // プッシュ通知表示
        login <-> app_push_msg_view
   }