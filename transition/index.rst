画面遷移設計
============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   web
   clientapp

.. todo:: 物理デザインと不整合な画面遷移が出た場合、物理デザインに合わせて修正する。
