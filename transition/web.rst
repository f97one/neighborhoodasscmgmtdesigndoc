Web側画面遷移
=============

町内会側
--------

「町内会管理者用ページ」にアクセスできるのは、ログインしたユーザーが以下の権限を有する場合のみである。

#. その町内会のシステム管理者
#. その町内会の役員

.. blockdiag::
   :scale: 50

   blockdiag web_transition {
       //orientation = portrait

       adv_page [label = "システム紹介ページ"];
       sign_up [label = "サインアップ"];

       login [label = "ログイン"];

       assoc_main_page [label = "町内会メインページ"];
       assoc_info_list [label = "町内会お知らせリスト"];
       assoc_info_detail [label = "町内会お知らせ詳細"];

       assoc_member_mypage [label = "町内会メンバーマイページ"];
       assoc_member_mypage_edit [label = "町内会メンバーマイページ編集"];
       assoc_member_mypage_confirm [label = "町内会メンバーマイページ編集確認"];
       assoc_member_mypage_comp [label = "町内会メンバーマイページ編集完了"];

       assoc_equipmgmt_main [label = "町内会備品管理メイン"];
       assoc_equipmgmt_reserve [label = "町内会備品貸出予約"];
       assoc_equipmgmt_reserve_confirm [label = "町内会備品貸出予約確認"];
       assoc_equipmgmt_reserve_comp [label = "町内会備品貸出予約完了"];
       assoc_equipmgmt_return [label = "町内会備品返却登録"];
       assoc_equipmgmt_return_confirm [label = "町内会備品返却登録確認"];
       assoc_equipmgmt_return_comp [label = "町内会備品返却登録完了"];

       assoc_income_report_view [label = "町内会収支報告表示"];
       assoc_income_mgmt_main [label = "町内会収支管理メイン"];
       assoc_income_mgmt_edit [label = "町内会収支管理編集"];
       assoc_income_mgmt_confirm [label = "町内会収支管理編集確認"];
       assoc_income_mgmt_comp [label = "町内会収支管理編集完了"];

       assoc_activities_report_view [label = "町内会活動報告表示"];
       assoc_activities_mgmt_main [label = "町内会活動報告メイン"];
       assoc_activities_mgmt_edit [label = "町内会活動報告編集"];
       assoc_activities_mgmt_confirm [label = "町内会活動報告編集確認"];
       assoc_activities_mgmt_comp [label ="町内会活動報告編集完了"];

       assoc_admin_main [label = "町内会管理者メイン"];
       assoc_admin_info_list [label = "町内会お知らせ管理メイン"];
       assoc_admin_info_msg_edit [label = "町内会お知らせ編集"];
       assoc_admin_info_msg_confirm [label = "町内会お知らせ確認"];
       assoc_admin_info_msg_comp [label = "町内会お知らせ登録完了"];
       assoc_admin_info_msg_read_inq [label = "町内会お知らせ既読統計"];

       assoc_admin_member_list [label = "町内会メンバー管理メイン"];
       assoc_admin_member_edit [label = "町内会メンバー編集"];
       assoc_admin_member_confirm [label = "町内会メンバー編集確認"];
       assoc_admin_member_comp [label = "町内会メンバー編集完了"];

       assoc_admin_push_list [label = "町内会プッシュ通知メイン"];
       assoc_admin_push_edit [label = "町内会プッシュ通知編集"];
       assoc_admin_push_confirm [label = "町内会プッシュ通知送信確認"];

       assoc_admin_equip_mgmt_main [label = "町内会備品管理メイン"];
       assoc_admin_equip_mgmt_edit [label = "町内会備品編集"];
       assoc_admin_equip_mgmt_confirm [label = "町内会備品編集確認"];
       assoc_admin_equip_mgmt_comp [label = "町内会備品編集完了"];

       assoc_dutytable_mgmt_main [label = "町内会ゴミステーション当番管理メイン"];
       assoc_dutytable_mgmt_edit [label = "町内会ゴミステーション当番管理編集"];
       assoc_dutytable_mgmt_confirm [label = "町内会ゴミステーション当番管理編集確認"];
       assoc_dutytable_mgmt_comp [label = "町内会ゴミステーション当番管理編集完了"];

       assoc_admin_assoc_config_main [label = "町内会固有設定メイン"];
       assoc_admin_assoc_config_edit [label = "町内会固有設定編集"];
       assoc_admin_assoc_config_confirm [label = "町内会固有設定編集確認"];
       assoc_admin_assoc_config_comp [label = "町内会固有設定編集完了"];

       adv_page <-> sign_up
       adv_page <-> login

       login -> assoc_main_page <-> assoc_info_list <-> assoc_info_detail
                assoc_main_page <-> assoc_info_detail
                assoc_main_page <-> assoc_income_report_view
                assoc_main_page <-> assoc_activities_report_view

                // マイページ
                assoc_main_page <-> assoc_member_mypage -> assoc_member_mypage_edit -> assoc_member_mypage_confirm -> assoc_member_mypage_comp -> assoc_member_mypage
                // 備品貸出／返却
                assoc_main_page <-> assoc_equipmgmt_main <-> assoc_equipmgmt_reserve -> assoc_equipmgmt_reserve_confirm -> assoc_equipmgmt_reserve_comp -> assoc_equipmgmt_main
                                    assoc_equipmgmt_main <-> assoc_equipmgmt_return -> assoc_equipmgmt_return_confirm -> assoc_equipmgmt_return_comp -> assoc_equipmgmt_main

                // 管理者ページ（町内会）
                assoc_main_page <-> assoc_admin_main

                    group assoc_admin_pages {
                        label = "町内会管理者用ページ"
                        color = "#dda0dd"

                        // メンバー管理
                        assoc_admin_main <-> assoc_admin_member_list -> assoc_admin_member_edit -> assoc_admin_member_confirm -> assoc_admin_member_comp -> assoc_admin_member_list
                        // お知らせ文書管理
                        assoc_admin_main <-> assoc_admin_info_list -> assoc_admin_info_msg_edit -> assoc_admin_info_msg_confirm -> assoc_admin_info_msg_comp -> assoc_admin_info_list
                                             assoc_admin_info_list <-> assoc_admin_info_msg_read_inq
                        // プッシュ通知発信
                        assoc_admin_main <-> assoc_admin_push_list -> assoc_admin_push_edit -> assoc_admin_push_confirm -> assoc_admin_push_list
                        // 備品管理
                        assoc_admin_main <-> assoc_admin_equip_mgmt_main -> assoc_admin_equip_mgmt_edit -> assoc_admin_equip_mgmt_confirm -> assoc_admin_equip_mgmt_comp -> assoc_admin_equip_mgmt_main
                        // 収支登録
                        assoc_admin_main <-> assoc_income_mgmt_main -> assoc_income_mgmt_edit -> assoc_income_mgmt_confirm -> assoc_income_mgmt_comp -> assoc_income_mgmt_main
                        // 活動報告管理
                        assoc_admin_main <-> assoc_activities_mgmt_main -> assoc_activities_mgmt_edit -> assoc_activities_mgmt_confirm -> assoc_activities_mgmt_comp -> assoc_activities_mgmt_main
                        // ゴミステーション当番管理
                        assoc_admin_main <-> assoc_dutytable_mgmt_main -> assoc_dutytable_mgmt_edit -> assoc_dutytable_mgmt_confirm -> assoc_dutytable_mgmt_comp -> assoc_dutytable_mgmt_main
                        // 町内会別設定
                        assoc_admin_main <-> assoc_admin_assoc_config_main -> assoc_admin_assoc_config_edit -> assoc_admin_assoc_config_confirm -> assoc_admin_assoc_config_comp -> assoc_admin_assoc_config_main
                    }
   }

運営管理側
----------

| 運営管理側画面へは、このアプリケーションの所有者、ならびに運営管理権限を有するもののみ、アクセスできる。  
| 「総合管理選択画面」では、総合管理機能への遷移と、自身が所属する町内会画面への遷移を選択できる。

.. blockdiag::
   :scale: 50

   blockdiag master_web_transition {
       login [label = "ログイン"];

       master_admin_selector [label = "総合管理選択画面"];
       master_admin_main [label = "総合管理者メニュー"];

       master_admin_assocgrp_list [label = "総合管理町内会設定メイン"];
       master_admin_assocgrp_edit [label = "総合管理町内会設定編集"];
       master_admin_assocgrp_confirm [label = "総合管理町内会設定編集確認"];
       master_admin_assocgrp_comp [label = "総合管理町内会編集完了"];

       master_admin_assocmember_main [label = "総合管理町内会メンバー設定メイン"];
       master_admin_assocmember_edit [label = "総合管理町内会メンバー編集"];
       master_admin_assocmember_confirm [label = "総合管理町内会メンバー編集確認"];
       master_admin_assocmember_comp [label = "総合管理町内会メンバー編集完了"];

       master_admin_info_list [label = "総合お知らせ管理メイン"];
       master_admin_info_msg_edit [label = "総合お知らせ編集"];
       master_admin_info_msg_confirm [label = "総合お知らせ確認"];
       master_admin_info_msg_comp [label = "総合お知らせ登録完了"];

       master_admin_push_list [label = "総合プッシュ通知メイン"];
       master_admin_push_edit [label = "総合プッシュ通知編集"];
       master_admin_push_confirm [label = "総合プッシュ通知送信確認"];

       master_admin_equip_mgmt_main [label = "総合備品管理メイン"];
       master_admin_equip_mgmt_edit [label = "総合備品編集"];
       master_admin_equip_mgmt_confirm [label = "総合備品編集確認"];
       master_admin_equip_mgmt_comp [label = "総合備品編集完了"];

       master_income_mgmt_main [label = "総合収支管理メイン"];
       master_income_mgmt_edit [label = "総合収支管理編集"];
       master_income_mgmt_confirm [label = "総合収支管理編集確認"];
       master_income_mgmt_comp [label = "総合収支管理編集完了"];

       master_activities_mgmt_main [label = "総合活動報告メイン"];
       master_activities_mgmt_edit [label = "総合活動報告編集"];
       master_activities_mgmt_confirm [label = "総合活動報告編集確認"];
       master_activities_mgmt_comp [label ="総合活動報告編集完了"];

       master_dutytable_mgmt_main [label = "総合ゴミステーション当番管理メイン"];
       master_dutytable_mgmt_edit [label = "総合ゴミステーション当番管理編集"];
       master_dutytable_mgmt_confirm [label = "総合ゴミステーション当番管理編集確認"];
       master_dutytable_mgmt_comp [label = "総合ゴミステーション当番管理編集完了"];

       master_config_main [label = "総合設定メイン"];
       master_config_edit [label = "総合設定編集"];
       master_config_confirm [label = "総合設定編集確認"];
       master_config_comp [label = "総合設定編集完了"];

       login -> master_admin_selector
            group master_admin_pages {
                label = "総合管理者用ページ"
                color = "#ffd700"

                // 町内会グループ設定
                master_admin_selector -> master_admin_main <-> master_admin_assocgrp_list -> master_admin_assocgrp_edit -> master_admin_assocgrp_confirm -> master_admin_assocgrp_comp -> master_admin_assocgrp_list
                                         // メンバー管理
                                         master_admin_main <-> master_admin_assocmember_main -> master_admin_assocmember_edit -> master_admin_assocmember_confirm -> master_admin_assocmember_comp -> master_admin_assocmember_main
                                         // 総合お知らせ文書管理
                                         master_admin_main <-> master_admin_info_list -> master_admin_info_msg_edit -> master_admin_info_msg_confirm -> master_admin_info_msg_comp -> master_admin_info_list
                                         // プッシュ通知管理
                                         master_admin_main <-> master_admin_push_list -> master_admin_push_edit -> master_admin_push_confirm -> master_admin_push_list
                                         // 備品管理
                                         master_admin_main <-> master_admin_equip_mgmt_main -> master_admin_equip_mgmt_edit -> master_admin_equip_mgmt_confirm -> master_admin_equip_mgmt_comp -> master_admin_equip_mgmt_main
                                         // 収支登録
                                         master_admin_main <-> master_income_mgmt_main -> master_income_mgmt_edit -> master_income_mgmt_confirm -> master_income_mgmt_comp -> master_income_mgmt_main
                                         // 活動報告管理
                                         master_admin_main <-> master_activities_mgmt_main -> master_activities_mgmt_edit -> master_activities_mgmt_confirm -> master_activities_mgmt_comp -> master_activities_mgmt_main
                                         // ゴミステーション当番管理
                                         master_admin_main <-> master_dutytable_mgmt_main -> master_dutytable_mgmt_edit -> master_dutytable_mgmt_confirm -> master_dutytable_mgmt_comp -> master_dutytable_mgmt_main
                                         // 全体設定
                                         master_admin_main <-> master_config_main -> master_config_edit -> master_config_confirm -> master_config_comp -> master_config_main
            }
   }

