この文書のライセンスについて
============================

.. image:: _static/cc-by-nc-nd_88x31.*
   :align: center
   :alt: "クリエイティブ・コモンズ・ライセンス"

この 文書 は `クリエイティブ・コモンズ 表示 - 非営利 - 改変禁止 4.0 国際 ライセンス <http://creativecommons.org/licenses/by-nc-nd/4.0/>`_ の下に提供されています。
