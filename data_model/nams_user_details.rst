NAMSユーザー詳細
================

概要
----

ユーザー認証は Firebase で行うが、ユーザーの所属町内会やアクセス権など Firebase Authentication で管理されない部分を管理するエンティティ。

物理名
------

NamsUserDetails

フィールド定義
--------------

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - username
     - ユーザー名
     - String
     - 1
     - 
     - 
     - unique
     - Firebase Authentication の UID を格納する
   * - primaryAssoc
     - 主所属町内会
     - String
     - 
     - false
     - ""(空文字)
     - 
     - デフォルトの空文字は「無所属」を示す
   * - enabled
     - 有効フラグ
     - Boolean
     - 
     - true
     - true
     - 
     - 
   * - createdAt
     - 作成日時
     - Date
     - 
     - true
     - now()
     - 
     - このレコードを作成した日時を格納する
   * - modefiedAt
     - 変更日時
     - Date
     - 
     - false
     - 
     - 
     - このレコードを更新した日時を格納する。作成しただけの場合はnull
   * - modefiedBy
     - 変更者
     - String
     - 
     - false
     - 
     - 
     - このレコードを作成、または変更したユーザーのUIDを格納する

外部キー制約
------------

.. list-table::
   :header-rows: 1

   * - キー物理名
     - 参照元カラム
     - 参照元カーディナリティ
     - 参照先カラム
     - 参照先カーディナリティ
     - 更新時オプション
     - 削除時オプション
   * - (自動)
     - NamsUserDetails.primaryAssoc
     - 0
     - NeighborhoodAssoc.assocId
     - 0 以上
     - 
     - 
