町内会役職
==========

概要
----

| 町内会の誰が、その役職についているかを管理するエンティティ。
| このエンティティにないユーザーは、役職を持たないものとする。

物理名
------

AssocPosition

フィールド定義
--------------

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - assocId
     - 町内会ID
     - String
     - 1
     - 
     - 
     -
     - 
   * - uid
     - ユーザー名
     - String
     - 2
     - 
     - 
     - 
     - 
   * - positionCode
     - 役職コード
     - Int
     - 
     - true
     - -1
     - 
     - アクセス権の根拠
   * - positionName
     - 役職表示名
     - String
     - 
     - false
     - 
     - 
     - 役職の表示名
   * - startsAt
     - 役職開始日時
     - Date
     - 
     - true
     - now()
     - 
     - 役職の開始日時
   * - expiresAt
     - 役職終了日時
     - Date
     - 
     - true
     - now() + 365日
     - 
     - 役職の終了予定日時
   * - enabled
     - 有効フラグ
     - Boolean
     - 
     - true
     - true
     - 
     - 
   * - createdAt
     - 作成日時
     - Date
     - 
     - true
     - now()
     - 
     - このレコードを作成した日時を格納する
   * - modefiedAt
     - 変更日時
     - Date
     - 
     - false
     - 
     - 
     - このレコードを更新した日時を格納する。作成しただけの場合はnull
   * - modefiedBy
     - 変更者
     - String
     - 
     - false
     - 
     - 
     - このレコードを作成、または変更したユーザーのUIDを格納する


外部キー制約
------------

.. list-table::
   :header-rows: 1

   * - キー物理名
     - 参照元カラム
     - 参照元カーディナリティ
     - 参照先カラム
     - 参照先カーディナリティ
     - 更新時オプション
     - 削除時オプション
   * - (自動)
     - NeighborhoodAssoc.assocId
     - 0
     - AssocPosition.assocId
     - 0 以上
     - 
     - 
   * - (自動)
     - NamsUserDetails.username
     - 0
     - AssocPosition.uid
     - 0 以上
     - 
     - 
