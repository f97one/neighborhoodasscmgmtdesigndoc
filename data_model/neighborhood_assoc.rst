町内会
======

概要
----

町内会の名称とIDを管理するエンティティ。

物理名
------

NeighborhoodAssoc

フィールド定義
--------------

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - assocId
     - 町内会ID
     - String
     - 1
     - 
     - 
     -
     - 初期投入時の「町内会名称__yyyyMMddHHmmss.SSS」をMD5に通した結果を、小文字の16進数文字列として格納する
   * - assocName
     - 町内会名称
     - String
     - 
     - true
     - ""(空文字)
     - 
     -
   * - enabled
     - 有効フラグ
     - Boolean
     - 
     - true
     - true
     - 
     - 
   * - createdAt
     - 作成日時
     - Date
     - 
     - true
     - now()
     - 
     - このレコードを作成した日時を格納する
   * - modefiedAt
     - 変更日時
     - Date
     - 
     - false
     - 
     - 
     - このレコードを更新した日時を格納する。作成しただけの場合はnull
   * - modefiedBy
     - 変更者
     - String
     - 
     - false
     - 
     - 
     - このレコードを作成、または変更したユーザーのUIDを格納する

外部キー制約
------------

なし
