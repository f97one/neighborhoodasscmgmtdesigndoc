.. NeighborhoodAssocMgmt documentation master file, created by
   sphinx-quickstart on Sun Jan 21 13:04:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

町内会管理システム（仮称）設計文書
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   00_license
   requirements
   transition/index
   operation_authority
   operation_flow/index
   data_model/index
   phys_design/index
   

.. todolist::

索引と目次
==========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
