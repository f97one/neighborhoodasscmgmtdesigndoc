収支報告関連機能
================

町内会費の出納記録と決算報告書の作成を行う機能。

本機能は、町内会管理チームの中でも町内会長、および会計担当のみ編集が可能で、会計監査の観点から総合管理者による処理代行は原則行わない。

収支報告表示
------------

全会員が、自身が所属する町内会の収支報告を表示できる。

.. seqdiag::

   seqdiag {
        "会員" -> "システム" [label = "表示したい年度を入力"];
        "会員" <- "システム" [label = "入力された年度の収支報告を表示"];
   }

出納記録登録
------------

| 町内会費の入金や使用金額の入力を行う。
| 町内会長、および会計担当のみ操作可能で、それ以外の会員はメニュー自体を非表示にしたうえで、権限チェックを行う。

.. seqdiag::

   seqdiag {
        "町内会管理チーム" -> "システム" [label = "出納記録登録を要求"];
        "町内会管理チーム" <- "システム" [label = "操作権限ありの場合登録画面を表示"];
        "町内会管理チーム" -> "システム" [label = "画面で出納記録を登録"];
   }

なお、記録訂正は訂正元となる記録に対して無効である旨を登録したのち、改めて訂正版記録を登録することで行う。
