操作フロー
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   subscribe_user
   equipment_mgmt
   manage_info
   balance_report
   activity_report
   garbage_station_duty
