ごみステーション当番管理機能
============================

あらかじめ設定したルートに従い、清掃や施錠の当番となっている会員に当番日程の通知を通知する。

当番の対応ができない場合は、事前通知からその旨を町内会管理チームに申し出ることもできることとする。

ごみステーション当番順路編集
----------------------------

会員の入退会による追加削除も、本機能で行う。

町内会で処理する場合
^^^^^^^^^^^^^^^^^^^^

.. seqdiag::

   seqdiag {
        "町内会管理チーム" -> "システム" [label = "当番グループにする会員を検索"];
        "町内会管理チーム" <- "システム" [label = "検索結果を返送"];
        "町内会管理チーム" -> "システム" [label = "検索をもとに当番グループと巡回パターンを設定"];
   }

総合管理者が処理を代行する場合
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

総合管理者に処理の代行を依頼する場合は、事前に町内会側で当番グループと巡回パターンを決定しておくものとする。

.. seqdiag::

   seqdiag {
        "町内会管理チーム"; "システム"; "総合管理者";

        "町内会管理チーム" -> "総合管理者" [label = "当番グループとなる会員のリストを通知"];
        "システム" <- "総合管理者" [label = "通知のあったグループと巡回パターンをシステムに反映"];
        "町内会管理チーム" <- "総合管理者" [label = "反映終了を通知"];
   }

ごみステーション当番通知
------------------------

cron で該当者を一日1回検索し、回収日の2週間前、1週間前、2日前にそれぞれ当番へ通知する。

当番が回収日に対応不能な場合、通知からその旨を町内会管理チームに通知することができる。

.. seqdiag::

   seqdiag {
        "会員"; "町内会管理チーム"; "システム";

        "システム" -> "システム" [label = "当番グループごとに当番を検索"];
        "会員" <- "システム" [label = "当番であることをプッシュ通知"];
        "会員" -> "町内会管理チーム" [label = "当番が対応不能な場合、その旨を通知"];
   }

ごみステーション当番一時組み換え
--------------------------------

次回分の予定 +のみ+ を一時的に組み替えるもので、回収日時の48時間より前の予定のみ一時組み換え対象とする。

この機能は、町内会管理チーム、または総合管理者のみ使用できる。

町内会で処理する場合
^^^^^^^^^^^^^^^^^^^^

.. seqdiag::

   seqdiag {
        "会員"; "町内会管理チーム"; "システム";

        "町内会管理チーム" -> "システム" [label = "一時組み換え対象の会員を検索"];
        "町内会管理チーム" <- "システム" [label = "対象者の当番日がいずれも直近の回収日時の48時間よりまえの場合のみ受付"];
        "町内会管理チーム" -> "システム" [label = "対象者の当番日を修正"];
        "会員" <- "システム" [label = "当番日が組み替えられたことを通知"];
   }

総合管理者が処理を代行する場合
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

事前に、

* 誰と
* 誰の
* いつの

当番日を一時組み換えするかを、町内会管理チームで確定しておくものとする。

.. seqdiag::

   seqdiag {
        "会員"; "町内会管理チーム"; "システム"; "総合管理者";

        "町内会管理チーム" -> "システム" [label = "一時組み換え対象の会員を検索"];
        "町内会管理チーム" <- "システム" [label = "対象者の当番日がいずれも直近の回収日時の48時間よりまえの場合のみ受付"];
        "町内会管理チーム" -> "総合管理者" [label = "対象者の一時組み換えを依頼"];
        "システム" <- "総合管理者" [label = "対象者の当番日を修正"];
        "町内会管理チーム" <- "システム" [label = "対象者の当番日組み換え終了を通知"]
        "会員" <- "システム" [label = "当番日が組み替えられたことを通知"];
   }
